import inspect
import unittest
from typing import Any
from unittest import mock

from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_rerunner, hoppr_process
from hoppr.exceptions import HopprError
from hoppr.result import Result
from hoppr.context import Context
from hoppr.result import Result
from hoppr.configs.credentials import Credentials
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component


class TestBasePlugin(unittest.TestCase):
    simple_test_context = Context(
        manifest="MANIFEST", 
        collect_root_dir="COLLECTION_DIR", 
        consolidated_sbom="BOM", 
        delivered_sbom="BOM", 
        retry_wait_seconds=1, 
        max_processes=3
    )

    class MyPlugin(HopprPlugin):

        supported_purl_types = ["good1", "good2"]


        def __init__(self, context: Context, config: Any):
            super().__init__(context=context, config=config)
            self.proc_comp_count = 0 # for counting the number of times process_component is called
            self.finalize_count = 0 # for counting the number of times post_stage_process is called

        @hoppr_rerunner
        def get_version(self):
            return "1.70.1"

        @hoppr_process
        @hoppr_rerunner
        def process_component(self, comp: Any) -> Result:
            self.proc_comp_count += 1
            if (self.config == "THROW_EXCEPTION"):
                raise HopprError("Throwing exception as specified by config value")
            return Result.retry(
                "my_plugin TestBasePlugin method get_component failed, but might succeed on a subsequent attempt if the anti-matter injectors come on-line."
            )

        @hoppr_process
        @hoppr_rerunner
        def post_stage_process(self):
            self.finalize_count += 1
            return Result.skip()

    class MyNoCompPlugin(HopprPlugin):

        supported_purl_types = ["good1", "good2"]

        def get_version(self):
            return "1.70.1"

    def test_plugin_base(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")

        assert my_plugin.supports_purl_type("good1") == True
        assert my_plugin.supports_purl_type("good2") == True
        assert my_plugin.supports_purl_type("bad") == False

        assert my_plugin.context.collect_root_dir == "COLLECTION_DIR"
        assert my_plugin.config == "CONFIG"
        assert my_plugin.context.manifest == "MANIFEST"

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert my_plugin.proc_comp_count == 3

        post_stage_process_result = my_plugin.post_stage_process()
        assert post_stage_process_result.is_skip(), f"Expected SKIP result, got {post_stage_process_result}"
        assert my_plugin.finalize_count == 1

    def test_plugin_base_exception(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="THROW_EXCEPTION")

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail()

    def test_plugin_base_bad_purl_type(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")

        comp = Component(name="TestComponent", purl="pkg:bad/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_skip()

    def test_plugin_base_bad_command(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")
        my_plugin.required_commands = ["doesnotexistcommand"]

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail()

    def test_plugin_base_default_iml(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyNoCompPlugin(context=self.simple_test_context, config="CONFIG")

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_skip()

        post_stage_process_result = my_plugin.post_stage_process()
        assert post_stage_process_result.is_skip()

    def test_retry_skip(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")

        my_plugin.create_logger()

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")

        result = my_plugin.post_stage_process()

        my_plugin.close_logger()

        assert result.is_skip()
        assert my_plugin.finalize_count == 1

    def test_retry_fail(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")

        my_plugin.create_logger()

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")

        result = my_plugin.process_component(comp)

        my_plugin.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Failure after 3 attempts, final message my_plugin TestBasePlugin method get_component failed, but might succeed on a subsequent attempt if the anti-matter injectors come on-line."

    def test_retry_not_result(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")

        my_plugin.create_logger()

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")

        result = my_plugin.get_version()

        my_plugin.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Method get_version returned str in rerunner. Result object required"

    def test_pre_stage_process(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyPlugin(context=self.simple_test_context, config="CONFIG")

        result = my_plugin.pre_stage_process()

        assert result.is_skip(), f"Expected SKIP result, got {result}"
