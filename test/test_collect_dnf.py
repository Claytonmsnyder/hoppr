import inspect
import os
import unittest
from pathlib import Path
from test.mock_objects import MockSubprocessRun
from unittest.mock import patch

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component

from hoppr import plugin_utils
from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.context import Context
from hoppr.core_plugins.collect_dnf_plugin import CollectDnfPlugin, _repo_proxy
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.manifest_file_content import Repository
from hoppr.hoppr_types.purl_type import PurlType
from hoppr.result import Result


class TestCollectorDnf(unittest.TestCase):

    def _create_test_plugin(self, *args, **kwargs):
        manifest = Manifest()
        manifest.consolidated_repositories = {
            PurlType.RPM: [Repository(url="https://somewhere.com", description="")]
        }

        context = Context(
            manifest=manifest, 
            collect_root_dir="COLLECTION_DIR", 
            consolidated_sbom="BOM", 
            delivered_sbom="BOM", 
            retry_wait_seconds=1, 
            max_processes=3
        )
        my_plugin = CollectDnfPlugin(context=context, config={"dnf_command": "dnf"}, *args, **kwargs)
        return my_plugin

    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test__get_found_repo_fail(self, mock_cmd_check):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self._create_test_plugin()
        mock_found_url = "https://somewhere.else.com/fakerpm-1.0.0-1.el8.noarch.rpm"

        self.assertIsNone(my_plugin._get_found_repo(mock_found_url))

    @patch("os.getenv")
    def test__repo_proxy(self, mock_getenv):
        mock_getenv.side_effect = lambda *x: {
            ("https_proxy", "_none_"): "https://proxy.somewhere.com",
            ("no_proxy", ""): "somewhere.else.com",
        }[x]

        self.assertEqual(_repo_proxy("https://mirror.somewhere.else.com/rpmtest-1.0.0-0.el8.x86_64.rpm"), "_none_")

    @patch("subprocess.run", return_value=MockSubprocessRun(rc=0, stdout=b"https://somewhere.com"))
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_collect_dnf_success(self, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}")

    @patch.object(Credentials, "find_credentials", return_value=CredObject("mock_user_name", "mock_password"))
    @patch("subprocess.run", return_value=MockSubprocessRun(rc=1))
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_collect_dnf_fail(self, mock_cmd_check, mock_subprocess_run, mock_get_creds):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail(), f"Expected FAIL result, got {collect_result}")
        self.assertEqual(collect_result.message, f"Failure after 3 attempts, final message dnf failed to locate package for {comp.purl}")

    def test_get_version(self):
        my_plugin = self._create_test_plugin()
        self.assertGreater(len(my_plugin.get_version()), 0)

    @patch("subprocess.run", return_value=MockSubprocessRun(rc=0))
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.fail("[mock] command not found"))
    def test_collect_dnf_command_not_found(self, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")

        plugin_utils.check_for_missing_commands.return_value = Result.fail("[mock] command not found")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail(), f"Expected FAIL result, got {collect_result}")
        self.assertTrue(collect_result.message == "[mock] command not found")

    @patch("subprocess.run", side_effect=[
        MockSubprocessRun(rc=0, stdout=b"https://somewhere.com"),
        MockSubprocessRun(rc=1),
        MockSubprocessRun(rc=0, stdout=b"https://somewhere.com"),
        MockSubprocessRun(rc=1),
        MockSubprocessRun(rc=0, stdout=b"https://somewhere.com"),
        MockSubprocessRun(rc=1),
        ])
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_collect_dnf_fail_download(self, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail(), f"Expected FAIL result, got {collect_result}")
        self.assertTrue(collect_result.message.startswith(f"Failure after 3 attempts, final message Failed to download DNF artifact"))

    @patch("subprocess.run", side_effect=[MockSubprocessRun(rc=0, stdout=b"[mock url]")])
    @patch.object(CollectDnfPlugin, "_get_found_repo", return_value=None)
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_collect_dnf_repo_not_found(self, mock_cmd_check, mock_component_path, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail())
        self.assertEqual(
            collect_result.message,
            f"Successfully found RPM file but URL does not match any repository in manifest. (Found URL: '[mock url]')"
        )

    @patch("subprocess.run", side_effect=[MockSubprocessRun(rc=0, stdout=b"[mock url]")])
    @patch.object(CollectDnfPlugin, "_get_found_repo", return_value="some.other.repo")
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_collect_dnf_url_mismatch(self, mock_cmd_check, mock_component_path, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64&repository_url=my.repo", type="file")
        collect_result = my_plugin.process_component(comp)

        self.assertTrue(collect_result.is_fail())
        self.assertEqual(
            collect_result.message,
            f"Purl-specified repository url (my.repo) does not match current repo (some.other.repo)."
        )

    @patch("subprocess.run", side_effect=[MockSubprocessRun(rc=0)])
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @patch.object(Credentials, "find_credentials", return_value=CredObject("mock_user_name", "mock_password"))
    def test_pre_stage_process_success(self, mock_credentials, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()
        pre_stage_process_result = my_plugin.pre_stage_process()

        self.assertTrue(pre_stage_process_result.is_success())

    @patch("subprocess.run", side_effect=[MockSubprocessRun(rc=1)])
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_pre_stage_process_fail_cache(self, mock_cmd_check, mock_subprocess_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self._create_test_plugin()
        pre_stage_process_result = my_plugin.pre_stage_process()

        self.assertTrue(pre_stage_process_result.is_fail())
        self.assertEqual(pre_stage_process_result.message, "Failed to populate DNF cache.")

    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    @patch.object(Path, "open", side_effect=OSError("Mock I/O error"))
    def test_pre_stage_process_write_file_fail(self, mock_open, mock_cmd_check):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        mock_config_file = Path("\\f\\a\\k\\e\\p\\a\\t\\h")
        my_plugin = self._create_test_plugin(config_file=mock_config_file)
        pre_stage_process_result = my_plugin.pre_stage_process()

        self.assertRaises(OSError, Path.open, mock_config_file)
        self.assertTrue(pre_stage_process_result.is_fail())

    @patch("shutil.copytree")
    @patch.object(Path, "rglob", return_value=[Path("/") / "var" / "tmp" / "dnf-temp" / "hoppr-tmp-0" / "repodata" ])
    @patch.object(Path, "unlink", return_value=None)
    @patch.object(Path, "exists", return_value=True)
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_post_stage_process_success(self, mock_cmd_check, mock_exists_check, mock_unlink, mock_rglob, mock_copytree):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self._create_test_plugin()

        mock_copytree.return_value = Path(
            my_plugin.context.collect_root_dir,
            "rpm",
            "https%3A%2F%2Fsomewhere.com%2F%24releasever%2F%24basearch",
            "repodata",
        )

        my_plugin.config_file.parent.mkdir(parents=True, exist_ok=True)
        my_plugin.config_file.write_text("[hoppr-tmp-0]\nbaseurl=https://somewhere.com/$releasever/$basearch\n")

        post_stage_process_result = my_plugin.post_stage_process()

        self.assertTrue(post_stage_process_result.is_success())

    @patch("shutil.copytree")
    @patch.object(Path, "rglob", return_value=[Path("/") / "var" / "tmp" / "dnf-temp" / "hoppr-tmp-0" / "repodata" ])
    @patch.object(Path, "unlink", side_effect=FileNotFoundError("Mock FileNotFoundError"))
    @patch.object(Path, "exists", return_value=True)
    @patch("hoppr.plugin_utils.check_for_missing_commands", return_value=Result.success())
    def test_post_stage_process_rm_file_fail(self, mock_cmd_check, mock_exists_check, mock_unlink, mock_rglob, mock_copytree):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self._create_test_plugin()

        mock_copytree.return_value = Path(
            my_plugin.context.collect_root_dir,
            "rpm",
            "https%3A%2F%2Fsomewhere.com%2F%24releasever%2F%24basearch",
            "repodata",
        )

        my_plugin.config_file.parent.mkdir(parents=True, exist_ok=True)
        my_plugin.config_file.write_text("[hoppr-tmp-0]\nbaseurl=https://somewhere.com/$releasever/$basearch\n")

        post_stage_process_result = my_plugin.post_stage_process()

        self.assertRaises(FileNotFoundError, Path.unlink, my_plugin.config_file)
        self.assertTrue(post_stage_process_result.is_fail())
