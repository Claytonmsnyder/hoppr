import inspect
import unittest

import pytest

from hoppr.context import Context


class TestContext(unittest.TestCase):
    def test_context(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        context = Context(manifest="MANIFEST", collect_root_dir="my_root", consolidated_sbom="BOM", delivered_sbom="BOM", max_processes=3)

        assert context.collect_root_dir == "my_root"
        assert context.manifest == "MANIFEST"
        assert context.consolidated_sbom == "BOM"
        assert context.max_attempts == 3
