import inspect
import unittest

from pathlib import Path
from packageurl import PackageURL
from typing import Any
from unittest.mock import patch

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component, Property

from hoppr.base_plugins.collector import (BatchCollectorPlugin,
                                          SerialCollectorPlugin)
from hoppr.base_plugins.hoppr import hoppr_rerunner
from hoppr.context import Context
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.purl_type import PurlType
from hoppr.result import Result


class TestCollectorBase(unittest.TestCase):

    simple_test_context = Context(
        manifest="MANIFEST", 
        collect_root_dir="COLLECTION_DIR", 
        consolidated_sbom="BOM", 
        delivered_sbom="BOM", 
        retry_wait_seconds=1, 
        max_processes=3
    )

    class MyBatchCollectorPlugin(BatchCollectorPlugin):

        supported_purl_types = ["good1", "good2"]

        def __init__(self, context: Context, config: Any):
            super().__init__(context=context, config=config)
            self.collect_count = 0 # for counting the number of times collect is called

        def get_version(self):
            return "1.70.1"

        @hoppr_rerunner
        def collect(self, comp: Any, repo_url: str, creds: CredObject = None) -> Result:
            self.collect_count += 1
            return Result.success()

    class MyCollectorPlugin(SerialCollectorPlugin):

        supported_purl_types = ["good1", "good2"]

        def __init__(self, context: Context, config: Any):
            super().__init__(context=context, config=config)
            self.collect_count = 0 # for counting the number of times collect is called

        def get_version(self):
            return "1.70.1"

        @hoppr_rerunner
        def collect(self, comp: Any, repo_url: str, creds: CredObject = None) -> Result:
            self.collect_count += 1
            return Result.success()

    class MyCollectorPluginRetry(SerialCollectorPlugin):

        supported_purl_types = ["good1", "good2"]

        def __init__(self, context: Context, config: Any):
            super().__init__(context=context, config=config)
            self.collect_count = 0 # for counting the number of times collect is called

        def get_version(self):
            return "1.70.1"

        @hoppr_rerunner
        def collect(self, comp: Any, repo_url: str, creds: CredObject = None) -> Result:
            self.collect_count += 1
            return Result.retry(
                "TestCollectorPlugin method collect failed, but might succeed on a subsequent attempt."
            )

    @patch.object(MyCollectorPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_base(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyCollectorPlugin(context=self.simple_test_context, config="CONFIG")

        assert my_plugin.context.collect_root_dir == "COLLECTION_DIR"
        assert my_plugin.config == "CONFIG"
        assert my_plugin.context.manifest == "MANIFEST"

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp=comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
        assert my_plugin.collect_count == 1

        test_dir = my_plugin.directory_for("pypi", "https://www.pypi.org/")
        assert test_dir == Path("COLLECTION_DIR","pypi","https%3A%2F%2Fwww.pypi.org")

        test_dir = my_plugin.directory_for("pypi", "https://www.pypi.org/", subdir="somewhere/deeper")
        assert test_dir == Path("COLLECTION_DIR","pypi","https%3A%2F%2Fwww.pypi.org","somewhere","deeper")

    @patch.object(MyCollectorPluginRetry, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_base_retry_fail(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyCollectorPluginRetry(context=self.simple_test_context, config="CONFIG")

        assert my_plugin.context.collect_root_dir == "COLLECTION_DIR"
        assert my_plugin.config == "CONFIG"
        assert my_plugin.context.manifest == "MANIFEST"

        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert my_plugin.collect_count == 3

    def test_get_repos(self):
        my_plugin = self.MyCollectorPluginRetry(context=self.simple_test_context, config="CONFIG")
        comp = Component(name="TestComponent", purl="pkg:good1/something/else@1.2.3", type="file")
        comp.properties = []
        comp.properties.append(Property(name="hoppr:repository:component_search_sequence", value='{"version": "v1", "Repositories": ["alpha", "beta", "gamma"]}'))

        assert my_plugin._get_repos(comp) == ['alpha', 'beta', 'gamma']

    @patch.object(MyCollectorPluginRetry, "_get_repos", return_value=["https://somewhere.com"])
    def test_collector_base_no_purl(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = self.MyCollectorPlugin(context=self.simple_test_context, config="CONFIG")

        comp = Component(name="TestComponent", purl=None, type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == "No purl supplied for component"

    @patch.object(Path, "is_dir", return_value=True)
    @patch.object(Path, "exists", return_value=True)
    def test_post_stage_process(self, mock_path_exists, mock_isdir):

        my_plugin = self.MyCollectorPlugin(context=self.simple_test_context, config="CONFIG")

        for purl_type in my_plugin.supported_purl_types:
            Path(my_plugin.context.collect_root_dir, purl_type).mkdir(parents=True, exist_ok=True)

        collect_result = my_plugin.post_stage_process()
        assert collect_result.is_success()

    def test_purl_type_string(self):
        self.assertEqual(str(PurlType.DOCKER), "docker")
        self.assertEqual(str(PurlType.GIT), "git")
        self.assertEqual(str(PurlType.GITHUB), "github")
        self.assertEqual(str(PurlType.GITLAB), "gitlab")
        self.assertEqual(str(PurlType.GOLANG), "golang")
        self.assertEqual(str(PurlType.HELM), "helm")
        self.assertEqual(str(PurlType.RPM), "rpm")
        self.assertEqual(str(PurlType.PYPI), "pypi")
        self.assertEqual(str(PurlType.MAVEN), "maven")
        self.assertEqual(str(PurlType.GENERIC), "generic")
        self.assertEqual(str(PurlType.APT), "apt")

    def test_check_purl_specified_url(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        result = SerialCollectorPlugin.check_purl_specified_url(PackageURL.from_string("pkg:generic/location?repository_url=http://my.repo:80"), "http://my.repo")
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        result = SerialCollectorPlugin.check_purl_specified_url(PackageURL.from_string("pkg:generic/location"), "https://my.repo")
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        result = SerialCollectorPlugin.check_purl_specified_url(PackageURL.from_string("pkg:generic/location?repository_url=my.repo:443"), "https://my.repo/")
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        result = SerialCollectorPlugin.check_purl_specified_url(PackageURL.from_string("pkg:generic/location?repository_url=my.repo"), "http://my.repo/with/more/stuff")
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        result = SerialCollectorPlugin.check_purl_specified_url(PackageURL.from_string("pkg:generic/location?repository_url=http://my.repo"), "https://your.repo")
        assert result.is_fail(), f"Expected FAIL result, got {result}"

        result = SerialCollectorPlugin.check_purl_specified_url(PackageURL.from_string("pkg:generic/location?repository_url=my.repo:22"), "ssh://my.repo")
        assert result.is_success(), f"Expected SUCCESS result, got {result}"
