import inspect
import unittest

import pytest
import requests

from hoppr.exceptions import HopprError
from hoppr.result import Result


def mock_response(code, message=""):
    response = requests.models.Response()
    response.status_code = code
    response._content = message.encode("utf-8")

    return response


class TestResult(unittest.TestCase):
    def test_success(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        result = Result.success()

        assert result.message == ""
        assert result.is_success()
        assert not result.is_retry()
        assert not result.is_fail()
        assert not result.is_skip()
        assert str(result) == "SUCCESS"

    def test_retry(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        result = Result.retry("Try again")

        assert result.message == "Try again"
        assert not result.is_success()
        assert result.is_retry()
        assert not result.is_fail()
        assert not result.is_skip()
        assert str(result) == "RETRY, msg: Try again"

    def test_fail(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        result = Result.fail("Sorry, Charlie")

        assert result.message == "Sorry, Charlie"
        assert not result.is_success()
        assert not result.is_retry()
        assert result.is_fail()
        assert not result.is_skip()
        assert str(result) == "FAIL, msg: Sorry, Charlie"

    def test_skip(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        result = Result.skip("N/A")

        assert result.message == "N/A"
        assert not result.is_success()
        assert not result.is_retry()
        assert not result.is_fail()
        assert result.is_skip()
        assert str(result) == "SKIP, msg: N/A"

    def test_from_http_success(self):
        resp = mock_response(200, "success")
        result = Result.from_http_response(resp)

        assert result.is_success()
        assert result.message == "HTTP Status Code: 200"

    def test_from_http_retry(self):
        resp = mock_response(500, "server error")
        result = Result.from_http_response(resp)

        assert result.is_retry()
        assert result.message == "HTTP Status Code: 500; server error"

    def test_from_http_fail(self):
        resp = mock_response(404, "not found")
        result = Result.from_http_response(resp)

        assert result.is_fail()
        assert result.message == "HTTP Status Code: 404; not found"

    def test_merge(self):
        init = Result.success()
        to_add = Result.fail("this failed")
        init.merge(to_add)

        assert init.is_fail()
        assert init.message == "this failed"

    def test_merge_skip(self):
        init = Result.success("it worked")
        to_add = Result.skip("this failed")
        init.merge(to_add)

        assert init.is_success()
        assert init.message == "it worked"

    def test_merge1(self):
        init = Result.success("it worked")
        to_add = Result.fail("this failed", return_obj="fail_obj")
        init.merge(to_add)

        assert init.is_fail()
        assert init.message == "it worked\nthis failed"
        assert init.return_obj == "fail_obj"

    def test_merge_raise_error(self):
        init = Result.success("it worked", return_obj="init obj")
        to_add = Result.fail("this failed", return_obj="fail_obj")

        with pytest.raises(HopprError):
            init.merge(to_add)

