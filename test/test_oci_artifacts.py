import json
import inspect
import unittest
from unittest import mock
from hoppr.configs.credentials import Credentials

import pytest

from pathlib import Path
import hoppr.plugin_utils
import hoppr.utils
import hoppr.net
from hoppr.exceptions import HopprLoadDataError
from test.test_utils import MockedResponse
from hoppr.hoppr_types.cred_object import CredObject
from hoppr import oci_artifacts


SAMPLE_BOM = Path(__file__).parent.joinpath("resources", "bom", "unit_bom3_mini.json")

class TestOciArtifacts(unittest.TestCase):
    @mock.patch("oras.provider.Registry.set_basic_auth")
    @mock.patch("oras.client.OrasClient.pull", return_value=[str(SAMPLE_BOM)])
    @mock.patch.object(
        Credentials, "find_credentials", return_value=CredObject("un", "pw")
    )
    def test_pull_artifact(self, mock_find_creds, mock_oras_pull, mock_oras_basic_auth):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        result = oci_artifacts.pull_artifact("registry.test.com/my/repo/image:1.2.3")
        assert result == hoppr.utils.load_file(SAMPLE_BOM)

        # Make sure our creds are provided to oras
        mock_oras_basic_auth.assert_called_once_with("un", "pw")
        mock_oras_pull.assert_called_once_with(
            target="registry.test.com/my/repo/image:1.2.3", outdir=None
        )

    @mock.patch("oras.provider.Registry.set_basic_auth")
    @mock.patch("oras.client.OrasClient.pull", return_value=[str(SAMPLE_BOM)])
    @mock.patch.object(Credentials, "find_credentials", return_value=None)
    def test_pull_artifact_to_disk(
        self, mock_find_creds, mock_oras_pull, mock_oras_basic_auth
    ):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        result = oci_artifacts.pull_artifact_to_disk("registry.test.com/my/repo/image:1.2.3", "/fake/out/dir")
        assert result == SAMPLE_BOM

        # Show that we don't crash if no creds exist - oras will try unauthorized access
        mock_oras_basic_auth.assert_not_called()
        mock_oras_pull.assert_called_once_with(
            target="registry.test.com/my/repo/image:1.2.3", outdir="/fake/out/dir"
        )


    @mock.patch("oras.provider.Registry.set_basic_auth")
    @mock.patch("oras.client.OrasClient.pull", return_value=[str(SAMPLE_BOM)])
    @mock.patch.object(
        Credentials, "find_credentials", return_value=CredObject("un", "pw")
    )
    def test_pull_artifact_discover_version_preserves_latest(
        self, mock_find_creds, mock_oras_pull, mock_oras_basic_auth
    ):
        result = oci_artifacts.pull_artifact("registry.test.com/my/repo/image:latest")
        assert result == hoppr.utils.load_file(SAMPLE_BOM)
        # This will verify it pull the higest version in the return tags
        mock_oras_pull.assert_called_once_with(
            target="registry.test.com/my/repo/image:latest", outdir=None
        )


    @mock.patch("oras.provider.Registry.set_basic_auth")
    @mock.patch("oras.client.OrasClient.pull", return_value=[str(SAMPLE_BOM)])
    @mock.patch(
        "oras.client.OrasClient.get_tags",
        return_value={
            "tags": ["main", "dev", "feature-branch", "0.1.1", "latest", "0.0.1", "0.0.2"]
        },
    )
    @mock.patch.object(
        Credentials, "find_credentials", return_value=CredObject("un", "pw")
    )
    def test_pull_artifact_discover_version_simple_semver(
        self, mock_find_creds, mock_get_tags, mock_oras_pull, mock_oras_basic_auth
    ):
        result = oci_artifacts.pull_artifact("registry.test.com/my/repo/image", allow_version_discovery=True)
        assert result == hoppr.utils.load_file(SAMPLE_BOM)
        # This will verify it pull the higest version in the return tags
        mock_oras_pull.assert_called_once_with(
            target="registry.test.com/my/repo/image:0.1.1", outdir=None
        )

    def test_pull_without_discovery_raises_error(self):
        with pytest.raises(HopprLoadDataError):
            oci_artifacts.pull_artifact("registry.test.com/my/repo/image", allow_version_discovery=False)

SEMVER_START_WITH_V = r"^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$"
# Explicitly not part of TestCase in order to allow use of parametrize
@pytest.mark.parametrize("version_list,expected_version,regex",[
    (["1.2.3", "1.2.3-rc.2", "1.0.0"], "1.2.3-rc.2", oci_artifacts.SEM_VER_FULL_REGEX),
    (["1.2.3", "1.2.3-rc.2", "2.0.0"], "2.0.0", oci_artifacts.SEM_VER_FULL_REGEX),
    (["1.2.3", "1.2.3-rc.2", "1.0.0"], "1.2.3", oci_artifacts.SEM_VER_CORE_REGEX),
    (["v1.2.3", "v1.2.3-rc.2", "v1.0.0"], "v1.2.3", SEMVER_START_WITH_V)
])
def test_discover_version_behavior(version_list, expected_version, regex):
    """Test the exact behavior of discover version with various tag patterns and different regexs"""
    with mock.patch(
        "oras.client.OrasClient.get_tags",
        return_value={"tags": version_list}):
            found_ver = oci_artifacts._discover_highest_version_tag("reg/my/artifact", version_regex=regex)
            assert found_ver == expected_version
