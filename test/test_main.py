import inspect
import pytest
import re
import unittest

from pathlib import Path
from unittest.mock import patch

import hoppr.main

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from hoppr_cyclonedx_models.cyclonedx_1_4 import \
    CyclonedxSoftwareBillOfMaterialsStandard as Bom

from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.configs.transfer import Transfer
from hoppr.processor import HopprProcessor
from hoppr.result import Result


test_component_list = [
        Component(
            name="transfer-test",
            type="file",
            purl="pkg:generic/README.md",
        ),
        Component(
            name="manifest",
            type="file",
            purl="pkg:generic/docs/CHANGELOG.md",
        ),
    ]

test_bom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX")
test_bom.components = test_component_list

class TestMain(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def capsys(self, capsys):
        self.capsys = capsys

    def test_verbose(self):
        verbose_flag = hoppr.main.verbose(verbose_flag=True)

        assert verbose_flag is True

    def test_version(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        hoppr.main.version()
        captured = self.capsys.readouterr()
        m = re.match(r".*\nHoppr Framework Version: .*\nPython Version: .*", captured.out)
        assert m is not None

    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_success(self, mock_transfer_load, mock_cred_load, mock_manifest_load, mock_copyfile, mock_flatten_sboms):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        hoppr.main.bundle("mock_manifest_file", "mock_creds_file", "mock_transfer_file", Path("mylog.txt"))

    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_fail_no_bom(self, mock_transfer_load, mock_cred_load, mock_manifest_load, mock_copyfile):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle("mock_manifest_file", "mock_creds_file", "mock_transfer_file", None)
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    @patch.object(HopprProcessor, 'run', return_value=Result.fail("oops"))
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_fail(self, mock_load_file, mock_cred_load, mock_manifest_load, mock_process_run):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle("mock_manifest_file", "mock_creds_file", "mock_transfer_file", None)
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1


    def test_validate(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        hoppr.main.validate([ Path("test","resources","manifest","unit","manifest.yml").resolve()],
            Path("test","resources","credential","cred-test.yml").resolve(),
            Path("test","resources","transfer","transfer-test.yml").resolve())
