import inspect
import tarfile
import unittest
from unittest import mock

from hoppr.context import Context
from hoppr.core_plugins.bundle_tar import TarBundlePlugin

class TestBundleTar(unittest.TestCase):

    simple_test_context = Context(
        manifest="MANIFEST", 
        collect_root_dir="COLLECTION_DIR", 
        consolidated_sbom="BOM", 
        delivered_sbom="BOM", 
        retry_wait_seconds=1, 
        max_processes=3
    )

    @mock.patch.object(tarfile, "open")
    def test_defaults(self, mock_tar_open):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config=None)

        result = my_plugin.post_stage_process()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"
        mock_tar_open.assert_called_once_with("./bundle.tar.gz", "x:gz")

        assert len(my_plugin.get_version()) > 0

    @mock.patch.object(tarfile, "open", side_effect=tarfile.ReadError())
    def test_no_comp_readerror(self, mock_tar_open):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"compression": "None"})

        result = my_plugin.post_stage_process()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        mock_tar_open.assert_called_once_with("./bundle.tar", "x:")

    @mock.patch.object(tarfile, "open", side_effect=FileNotFoundError())
    def test_xz_filenotfound(self, mock_tar_open):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"compression": "lzma"})

        result = my_plugin.post_stage_process()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        mock_tar_open.assert_called_once_with("./bundle.tar.xz", "x:xz")

    @mock.patch.object(tarfile, "open", side_effect=PermissionError())
    def test_xz_permission(self, mock_tar_open):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"compression": "lzma"})

        result = my_plugin.post_stage_process()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        mock_tar_open.assert_called_once_with("./bundle.tar.xz", "x:xz")

    @mock.patch.object(tarfile, "open")
    def test_bad_compression(self, mock_tar_open):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"compression": "bad"})

        result = my_plugin.post_stage_process()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        mock_tar_open.assert_not_called()

    @mock.patch("time.strftime", return_value="20200202-200202")
    @mock.patch("os.path.exists", return_value=True)
    @mock.patch.object(tarfile, "open")
    def test_fileexists(self, mock_tar_open, mock_file_exists, mock_strftime):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"compression": "bz", "tarfile_name": "my.tarfile.tar.bz"})

        result = my_plugin.post_stage_process()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"
        mock_tar_open.assert_called_once_with("my.tarfile-20200202-200202.tar.bz", "x:bz2")

    @mock.patch("time.strftime", return_value="20200202-200202")
    @mock.patch("os.path.exists", return_value=True)
    @mock.patch.object(tarfile, "open")
    def test_fileexists_no_comp(self, mock_tar_open, mock_file_exists, mock_strftime):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"compression": "none", "tarfile_name": "my.tarfile.tar"})

        result = my_plugin.post_stage_process()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"
        mock_tar_open.assert_called_once_with("my.tarfile-20200202-200202.tar", "x:")

    @mock.patch("time.strftime", return_value="20200202-200202")
    @mock.patch("os.path.exists", return_value=True)
    @mock.patch.object(tarfile, "open")
    def test_fileexists_noext(self, mock_tar_open, mock_file_exists, mock_strftime):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        my_plugin = TarBundlePlugin(context=self.simple_test_context, config={"tarfile_name": "my.tarfile"})

        result = my_plugin.post_stage_process()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"
        mock_tar_open.assert_called_once_with("my.tarfile-20200202-200202", "x:gz")
