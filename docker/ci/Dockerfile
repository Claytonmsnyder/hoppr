ARG BASE_IMAGE=docker.io/library/ubuntu
ARG BASE_TAG=jammy-20220428
ARG POETRY_HOME=/opt/poetry
ARG POETRY_VERSION=1.2.0

FROM $BASE_IMAGE:$BASE_TAG AS builder

ARG APT_PKGS="curl git jq python3-pip python3.10 python3.10-venv"
ARG NODE_VERSION=v16.9.1
ARG NODE_DOWNLOAD_URL=https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.xz
ARG POETRY_HOME
ARG POETRY_VERSION

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:${POETRY_HOME}/bin

# Install base dependencies
RUN apt-get update && apt-get install --yes ${APT_PKGS}

# Install Node.JS, npm, yarn, yq
RUN update-ca-certificates --fresh \
    && curl --fail --silent --show-error --location --url ${NODE_DOWNLOAD_URL} | tar --extract --xz \
    && for node_dir in bin include lib share; do \
          cp -r node-${NODE_VERSION}-linux-x64/${node_dir} /usr; \
        done \
    && rm -r node-${NODE_VERSION}-linux-x64 \
    && npm install --global yarn \
    && curl --silent --location --output /usr/local/bin/yq --url https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 \
    && chmod +x /usr/local/bin/yq

# Install poetry
RUN python3 -m venv ${POETRY_HOME} \
    && export PIP_TRUSTED_HOST="pypi.org pypi.python.org files.pythonhosted.org" \
    && ${POETRY_HOME}/bin/pip install --no-cache-dir --upgrade pip setuptools \
    && ${POETRY_HOME}/bin/pip install --no-cache-dir poetry==${POETRY_VERSION}

# Flatten build layers into single layer
FROM $BASE_IMAGE:$BASE_TAG
ARG POETRY_HOME

COPY --from=builder / /

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:${POETRY_HOME}/bin

CMD ["/usr/bin/bash"]
